"""AMQP webhook handler."""
import argparse
import os
import sys

from cki_lib import messagequeue
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
import sentry_sdk

from . import helpers
from . import umb

LOGGER = get_logger('cki.umb_messenger.webhook')


def handle_job_event(pipeline_id, project_info, umb_config, gitlab_instance,
                     job_name=None):
    """Handle a GitLab job event.

    Check if this event means all "setup" stages have succeeded and if so, send
    a UMB message if it was requested.
    """
    LOGGER.debug('Handling job event from pipeline %s', pipeline_id)

    project = gitlab_instance.projects.get(project_info)
    pipeline = project.pipelines.get(pipeline_id)
    variables = helpers.get_variables(pipeline)

    if helpers.should_skip(variables):
        LOGGER.debug('Skipping pipeline!')
        return

    # Should we send a message for this pipeline?
    send_message = misc.strtobool(variables.get('send_ready_for_test_pre',
                                                'False'))
    if not send_message:
        LOGGER.debug('Pipeline not configured for pre-test messages.')
        return

    # Check if *all* setup jobs completed and if this is the last one.
    successful_jobs = helpers.all_jobs_succeeded(
        project, pipeline, 'setup', job_name
    )
    if not successful_jobs:
        LOGGER.debug('%s not ready for pre-test notifications.',
                     pipeline.attributes['web_url'])
        return

    umb.handle_message('ready_for_test', project, pipeline, umb_config, 'pre')


def handle_finished_pipeline(pipeline_id, project_name, umb_config,
                             gitlab_instance):
    """Send an UMB message for finished pipeline, if it was requested."""
    LOGGER.debug('Handling any messaging for pipeline %s', pipeline_id)

    project = gitlab_instance.projects.get(project_name)
    pipeline = project.pipelines.get(pipeline_id)
    variables = helpers.get_variables(pipeline)

    if helpers.should_skip(variables):
        LOGGER.debug('Skipping pipeline!')
        return

    # Should we send a post-test message?
    send_message = misc.strtobool(variables.get('send_ready_for_test_post',
                                                'False'))
    if not send_message:
        LOGGER.debug('Pipeline not configured for post-test messages.')
    else:
        umb.handle_message('ready_for_test',
                           project,
                           pipeline,
                           umb_config,
                           message_sub_type='post')

    # Should we send a message to OSCI?
    if 'osci' in variables.get('report_types', '').split(','):
        umb.handle_message('osci', project, pipeline, umb_config)


def process_message(umb_config, payload):
    """Webhook handler."""
    if payload['object_kind'] == 'build':
        # Completely skip handling messages we don't care about.
        if payload['build_stage'] == 'setup' and \
                payload['build_status'] == 'success':
            # Use project_id as project_name attribute is mangled in job
            # events. path_with_namespace from pipeline event is correct
            # (cki-project/cki-pipeline) but the project_name here is
            # "CKI Project / cki-pipeline" which doesn't work well with the
            # API.
            handle_job_event(payload['pipeline_id'],
                             payload['project_id'],
                             umb_config,
                             messagequeue.Message(payload).gl_instance(),
                             payload['build_name'])
    elif payload['object_kind'] == 'pipeline':
        status = payload['object_attributes']['status']
        if status in ['failed', 'success']:
            handle_finished_pipeline(
                payload['object_attributes']['id'],
                payload['project']['path_with_namespace'],
                umb_config,
                messagequeue.Message(payload).gl_instance(),
            )
    else:
        LOGGER.warning('Unknown hook type: %s',
                       payload['object_kind'])


def process_cli(pipeline_id, project_name, umb_config, gitlab_instance,
                hook_type):
    """CLI handler."""
    if hook_type == 'job':
        handle_job_event(
            pipeline_id, project_name, umb_config, gitlab_instance
        )
    elif hook_type == 'pipeline':
        handle_finished_pipeline(
            pipeline_id, project_name, umb_config, gitlab_instance
        )
    else:
        LOGGER.info('Unknown hook type: %s', hook_type)


def main(args):
    """Run main loop."""
    umb_config = umb.load_configs()

    parser = argparse.ArgumentParser(
        description='Handle UMB pipeline results.')
    parser.add_argument('--queue', action='store_true',
                        help='Retrieve webhook data from an AMQP queue')
    parser.add_argument('--gitlab-url',
                        help='Simulate a webhook for the given GitLab URL')
    parser.add_argument('--hook-type',
                        help='Simulate a webhook with the given type')
    parser.add_argument('--id', type=int,
                        help='Simulate a webhook for the given pipeline id')
    parser.add_argument('--project',
                        help='Simulate a webhook for the given project')
    args = parser.parse_args(args)

    if args.gitlab_url and args.hook_type and args.id and args.project:
        process_cli(args.id, args.project, umb_config,
                    get_instance(args.gitlab_url), args.hook_type)
    elif args.queue:
        if misc.is_production():
            sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))

        messagequeue.MessageQueue().consume_messages(
            os.environ.get('WEBHOOK_RECEIVER_EXCHANGE',
                           'cki.exchange.webhooks'),
            os.environ['UMB_MESSENGER_ROUTING_KEYS'].split(),
            lambda _, p: process_message(umb_config, p),
            queue_name=os.environ.get('UMB_MESSENGER_QUEUE'))
    else:
        print('Either --queue or --gitlab-url/hook-type/id/project needed')
        sys.exit(1)
