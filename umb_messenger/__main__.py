"""Main entrypoint."""
import sys

from . import main

if __name__ == "__main__":
    main.main(sys.argv[1:])
