"""
Tests for umb_messenger.pipeline_data. Mostly sanity checks to verify content
is set in the correct function.
"""
import copy
import os
import unittest
from unittest import mock

from tests import fakes
from umb_messenger import pipeline_data


class TestFillCommonData(unittest.TestCase):
    """Tests for pipeline_data.fill_common_data()."""
    @mock.patch('umb_messenger.helpers.get_variables')
    def test_sanity(self, mock_variables):
        """Sanity check to verify expected fields are set."""
        message = {'system': [{'os': None}],
                   'run': {'url': None},
                   'generated_at': None}
        mock_variables.return_value = {'cki_pipeline_branch': 'fedora'}
        pipe = fakes.FakePipeline('success', 1)
        pipe.attributes['web_url'] = 'pipe_url'

        test_message = pipeline_data.fill_common_data(message, pipe)

        self.assertIsNot(test_message['generated_at'], None)
        self.assertIsNot(test_message['system'][0]['os'], None)
        self.assertIsNot(test_message['run']['url'], None)


class TestFillBaseOSCIData(unittest.TestCase):
    """Tests for pipeline_data.fill_base_osci_data()."""
    @mock.patch('umb_messenger.helpers.get_variables')
    def test_sanity(self, mock_variables):
        """Sanity check to verify expected fields are set."""
        message = {'artifact': {'issuer': None,
                                'id': None,
                                'nvr': None,
                                'scratch': None}}
        mock_variables.return_value = {'owner': 'me',
                                       'brew_task_id': 123,
                                       'nvr': 'kernel-123.src.rpm',
                                       'is_scratch': 'False'}
        pipe = fakes.FakePipeline('success', 1)

        test_message = pipeline_data.fill_base_osci_data(message, pipe)

        self.assertIsNot(test_message['artifact']['issuer'], None)
        self.assertIsNot(test_message['artifact']['id'], None)
        self.assertIsNot(test_message['artifact']['nvr'], None)
        self.assertFalse(test_message['artifact']['scratch'])

        # Besides checking the value is set, also check the suffix is removed
        # from NVR
        self.assertEqual(test_message['artifact']['nvr'], 'kernel-123')

        # Test also the 'else' branch
        mock_variables.return_value['is_scratch'] = 'True'
        test_message = pipeline_data.fill_base_osci_data(message, pipe)
        self.assertTrue(test_message['artifact']['scratch'])


class TestGatingData(unittest.TestCase):
    """Tests for pipeline_data.get_gating_data()."""
    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_missing_architecture(self, mock_data, mock_jobs):
        """Verify error message is set if architecture is missing."""
        message = {'system': [{'architecture': None}]}
        job = fakes.FakeProjectJob(1, 1, {'web_url': 'url'})

        mock_jobs.return_value = [job]
        mock_data.return_value = {}

        messages = pipeline_data.get_gating_data(message, None, None)

        self.assertEqual(len(messages), 1)
        self.assertIn('architecture for url', messages[0]['reason'])

    @mock.patch.dict(os.environ, {'BEAKER_URL': 'beaker.url'})
    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_fields(self, mock_data, mock_jobs):
        """
        Verify debug kernel info is added to the message type and that all
        expected fields are set.
        """
        message = {'system': [{'architecture': None, 'os': 'fedora'}],
                   'type': None,
                   'status': None,
                   'artifact': {'nvr': 'kernel-123'},
                   'run': {'log': None, 'debug': None, 'rebuild': None}}

        pipe = fakes.FakePipeline('success', 1, {})
        job = fakes.FakeProjectJob(2, 2, {})

        mock_jobs.return_value = [job]
        mock_data.return_value = {'retcode': 0,
                                  'debug_kernel': 'yes',
                                  'kernel_arch': 'x86_64',
                                  'kernel_version': '123'}

        messages = pipeline_data.get_gating_data(message, pipe, None)

        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]['type'], 'tier1-x86_64-debug')
        self.assertEqual(messages[0]['status'], 'passed')
        self.assertIn('123%40fedora', messages[0]['run']['log'])

        # Verify non-debug kernel values
        mock_data.return_value['debug_kernel'] = 'no'
        messages = pipeline_data.get_gating_data(message, pipe, None)
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]['type'], 'tier1-x86_64')

        # Verify field values with a failure retcode
        mock_data.return_value['retcode'] = 1
        messages = pipeline_data.get_gating_data(message, pipe, None)
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]['status'], 'failed')

        # Verify Beaker errors
        mock_data.return_value['retcode'] = 2
        messages = pipeline_data.get_gating_data(message, pipe, None)
        self.assertEqual(len(messages), 1)
        self.assertIn('Missing test results', messages[0]['reason'])


class TestReadyForTestData(unittest.TestCase):
    """Tests for pipeline_data.fill_ready_for_test_data()."""
    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_pre_test(self, mock_data, mock_jobs):
        """Verify expected values for pre-test notifications are set."""
        template = {'system': [{'os': 'rhel7',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None}
        pipe = fakes.FakePipeline('running', 1, {'patch_urls': 'url1 url2',
                                                 'submitter': 'someone',
                                                 'mr_url': 'mr-url'})

        mock_jobs.return_value = ['job1', 'job2']
        rc_data = [
            {'debug_kernel': 'yes',
             'kernel_arch': 'x86_64',
             'kernel_package_url': 'kernel1_url',
             'modified_files': 'file1',
             'stream': 'rhel78-z'},
            {'debug_kernel': 'no',
             'kernel_arch': 'x86_64',
             'kernel_package_url': 'kernel2_url',
             'modified_files': 'file1',
             'stream': 'rhel78-z'}
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'pre', pipe, None
        )

        # Check basic data
        self.assertEqual(message['pipelineid'], 1)
        self.assertEqual(message['artifact']['issuer'], 'someone')
        self.assertFalse(message['cki_finished'])
        self.assertNotIn('status', message)
        self.assertEqual(message['patch_urls'], ['url1', 'url2'])
        self.assertEqual(message['modified_files'], ['file1'])
        self.assertEqual(message['system'][0]['stream'], 'rhel78-z')

        # Check MR URL
        self.assertEqual(message['merge_request_url'], 'mr-url')

        # Check build info
        self.assertEqual(len(message['build_info']), 2)
        self.assertEqual(
            message['build_info'][0],
            {'architecture': 'x86_64',
             'debug_kernel': True,
             'kernel_package_url': 'kernel1_url'}
        )
        self.assertEqual(
            message['build_info'][1],
            {'architecture': 'x86_64',
             'debug_kernel': False,
             'kernel_package_url': 'kernel2_url'}
        )

        # Check git builds instead of patches
        pipe.variables._vars = []
        del rc_data[0]['modified_files']
        del rc_data[1]['modified_files']
        mock_data.side_effect = rc_data
        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'pre', pipe, None
        )

        self.assertEqual(message['artifact']['issuer'], 'CKI')
        self.assertEqual(message['patch_urls'], [])
        self.assertEqual(message['modified_files'], [])

        # Check MR pipelines
        rc_data[0]['mr_diff_url'] = 'link-to-diff'
        mock_data.side_effect = rc_data
        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'pre', pipe, None
        )
        self.assertEqual(message['patch_urls'], ['link-to-diff'])

    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_post_test(self, mock_data, mock_jobs):
        """
        Verify expected values for post-test notifications are set. We don't
        need to test both patch and git builds as that's already done for
        pre-test messages and the code is same for both cases.
        """
        template = {'system': [{'os': 'mainline.kernel.org',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None}
        pipe = fakes.FakePipeline('finished', 1, {'tree_name': 'upstream'})

        mock_jobs.return_value = ['job1', 'job2']
        rc_data = [
            {'debug_kernel': 'yes',
             'kernel_arch': 'x86_64',
             'kernel_package_url': 'kernel1_url',
             'retcode': 0},
            {'debug_kernel': 'no',
             'kernel_arch': 'x86_64',
             'kernel_package_url': 'kernel2_url',
             'retcode': 0}
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )

        # Check basic data
        self.assertTrue(message['cki_finished'])
        self.assertEqual(message['pipelineid'], 1)
        self.assertEqual(message['artifact']['issuer'], 'CKI')
        self.assertEqual(message['status'], 'success')
        self.assertEqual(message['patch_urls'], [])
        self.assertEqual(message['modified_files'], [])
        self.assertEqual(message['system'][0]['stream'], 'upstream')

        # Check build info
        self.assertEqual(len(message['build_info']), 2)
        self.assertEqual(
            message['build_info'][0],
            {'architecture': 'x86_64',
             'debug_kernel': True,
             'kernel_package_url': 'kernel1_url'}
        )
        self.assertEqual(
            message['build_info'][1],
            {'architecture': 'x86_64',
             'debug_kernel': False,
             'kernel_package_url': 'kernel2_url'}
        )

        # Verify status if any tests failed
        rc_data[1]['retcode'] = 1
        mock_data.side_effect = rc_data
        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )
        self.assertEqual(message['status'], 'fail')

    @mock.patch('umb_messenger.helpers.get_newest_jobs')
    @mock.patch('umb_messenger.helpers.get_rc_data')
    def test_missing_data(self, mock_data, mock_jobs):
        """
        Verify we don't send messages for builds we can't get info for.
        The cases are:
            - missing architecture info
            - missing kernel URL
            - no test data, if we're sending post-test message
        """
        template = {'system': [{'os': 'fedora',
                                'stream': None}],
                    'pipelineid': None,
                    'artifact': {'issuer': None},
                    'cki_finished': None,
                    'status': None,
                    'patch_urls': None,
                    'build_info': None,
                    'modified_files': None}
        pipe = fakes.FakePipeline('finished', 1, {})

        mock_jobs.return_value = [fakes.FakeProjectJob(2, 2, {}),
                                  fakes.FakeProjectJob(3, 3, {}),
                                  fakes.FakeProjectJob(4, 4, {})]
        # Make each job miss only one attribute, to verify skipping works
        # correctly
        rc_data = [
            {'debug_kernel': 'yes',
             'kernel_arch': 'x86_64',
             'retcode': 1},
            {'debug_kernel': 'no',
             'kernel_package_url': 'kernel1_url',
             'retcode': 0},
            {'debug_kernel': 'no',
             'kernel_package_url': 'kernel2_url',
             'kernel_arch': 'x86_64',
             'retcode': 2}
        ]
        mock_data.side_effect = rc_data

        message = copy.deepcopy(template)
        message = pipeline_data.fill_ready_for_test_data(
            message, 'post', pipe, None
        )

        self.assertIn('No test data found', message['reason'])
