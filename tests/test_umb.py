"""Tests for umb_messenger.umb."""
# pylint: disable=no-self-use
import json
import unittest
from unittest import mock

from tests import fakes
from umb_messenger import umb


class TestHandleMessage(unittest.TestCase):
    """Tests for umb.handle_message()."""

    def test_unsupported(self):
        """Verify functionality for unsupported branches."""
        pipe = fakes.FakePipeline('success', 1, {})
        pipe.attributes['ref'] = 'unsupported'

        with self.assertLogs(umb.LOGGER, 'INFO') as log:
            umb.handle_message('osci', None, pipe, {})
            self.assertIn('No UMB settings', log.output[-1])

    @mock.patch('umb_messenger.pipeline_data.get_gating_data')
    @mock.patch('umb_messenger.pipeline_data.fill_base_osci_data')
    @mock.patch('umb_messenger.pipeline_data.fill_common_data')
    @mock.patch('stomp.Connection')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_osci(self, mock_connection, mock_common, mock_base_osci,
                  mock_gating):
        """Sanity check for OSCI messages."""
        pipe = fakes.FakePipeline('success', 1, {})
        pipe.attributes['ref'] = 'fedora'
        config = {'report_fedora': {'brokers': {'url': 'port'},
                                    'osci.error': 'error topic',
                                    'osci.complete': 'complete topic'}}

        message_good = {'message1': 'good'}
        message_bad = {'reason': 'this went wrong'}
        mock_gating.return_value = [message_good, message_bad]

        umb.handle_message('osci', None, pipe, config)

        mock_common.assert_called_once()
        mock_base_osci.assert_called_once()
        mock_gating.assert_called_once()

        # Verify both messages and topics are handled correctly
        mock_connection.return_value.send.assert_has_calls([
            mock.call('complete topic', json.dumps(message_good)),
            mock.call('error topic', json.dumps(message_bad))
        ])

    @mock.patch('umb_messenger.pipeline_data.fill_ready_for_test_data')
    @mock.patch('umb_messenger.pipeline_data.fill_common_data')
    @mock.patch('stomp.Connection')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_ready_for_test(self, mock_connection, mock_common, mock_ready):
        """
        Sanity check for ready_for_test messages. The behavior is same for both
        pre- and post-test messages so we don't need to test both.
        """
        pipe = fakes.FakePipeline('success', 1, {})
        pipe.attributes['ref'] = 'fedora'
        config = {'ready_for_test': {'brokers': {'url': 'port'},
                                     'ready_for_test.complete': 'topic'}}

        message_good = {'message1': 'good'}
        mock_ready.return_value = message_good
        umb.handle_message('ready_for_test', None, pipe, config, 'pre')

        # Verify "good" message and topic is handled correctly
        mock_connection.return_value.send.assert_called_with(
            'topic', json.dumps(message_good)
        )
        mock_common.assert_called_once()

        mock_ready.return_value = {'reason': 'this went wrong'}
        with self.assertLogs(umb.LOGGER, 'INFO') as log:
            umb.handle_message('ready_for_test', None, pipe, config, 'pre')
            self.assertIn('No topic configured', log.output[-1])
